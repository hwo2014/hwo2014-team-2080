#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <list>
#include <limits>
#include <jsoncons/json.hpp>

#define DECISION_LOGGING
//#define CSV_LOGGING

enum section_type {
  STRAIGHT,
  TURN
};

struct track_section {

  // Start piece index
  int start_index;

  // End piece index, this is the starting index of the next piece
  // (this index is not part of the section)
  int end_index;

  int lane;
  
  // Maximum entry speed to this section
  double max_entry_speed;

  // Section type
  section_type type;

  // If this is a turn section, total turn angle (for straights, this isn't used)
  double total_angle;

  // Total length of the section (for turns, this isn't used)
  // TODO: should be an array of lengths, for each lane
  double total_length;

  track_section(int start_index,
                int end_index,
                int lane,
                section_type type,
                double total_length,
                double total_angle,
                double max_entry_speed = std::numeric_limits<int>::max()):
    start_index(start_index),
    end_index(end_index),
    lane(lane),
    type(type),
    total_angle(total_angle),
    total_length(total_length),
    max_entry_speed(max_entry_speed)
  {
    
  };
  
  friend std::ostream& operator<<(std::ostream& out, const track_section& section) // output
  {
    out << "section (" << section.start_index << ", " << section.end_index << ") lane " << section.lane;
    if (section.type == section_type::STRAIGHT) {
      out << " straight: " << section.total_length << " length";
    } else if (section.type == section_type::TURN) {        
      out << " turn: " << section.total_length << " length, " << section.total_angle << " degrees";
    }
    
    return out;
  }
  
};
  
struct car_position {
  double angle;
  int piece_index;
  double in_piece_distance;
  int start_lane_index;
  int end_lane_index;
  int lap;
  double velocity;

  car_position():
    angle(0.d),
    piece_index(0),
    in_piece_distance(0.d),
    start_lane_index(0),
    end_lane_index(0),
    lap(0),
    velocity(0.d)
  {};
  
  /**
   * Update all fields from the JSON car positions message
   */
  void update_from_json(const jsoncons::json& car_data);

  /**
   * Calculate the distance between two consecutive car positions
   */
  double calculate_distance(jsoncons::json* track_data, const car_position& previous_position);
    
};

class game_logic
{
public:
  typedef std::vector<jsoncons::json> msg_vector;

  game_logic();
  ~game_logic();
  msg_vector react(const jsoncons::json& msg); 

  /**
   * Get the track length of a turn piece, given a lane index.
   * TODO: For turns that include a switch, that is not accounted for.
   */
  static double get_turn_piece_length(
    jsoncons::json* track_data,
    int piece_index,
    int lane_index);
  
private:
  
  static std::string LEFT_TURN_STR;
  static std::string RIGHT_TURN_STR;
  
  typedef std::function<msg_vector(game_logic*, const jsoncons::json&, const int gameTick)> action_fun;
  const std::map<std::string, action_fun> action_map;

  jsoncons::json* m_track_data;

  std::string m_car_name;
  int m_queued_switch_index;

  car_position m_last_position;
  car_position m_current_position;

  bool m_race_started;

  bool m_turbo_ready;
  bool m_turbo_on;

  int m_total_laps;
  
  int* m_optimal_lanes;
  double* m_lookahead_angles;
  double m_last_throttle;

  // Track section list, one per lane
  int m_current_track_section;
  std::vector<track_section>* m_track_sections;
  
#ifdef CSV_LOGGING
  std::ofstream m_csv_log;
#endif

  msg_vector on_join(const jsoncons::json& data, const int gameTick);
  msg_vector on_join_race(const jsoncons::json& data, const int gameTick);
  msg_vector on_your_car(const jsoncons::json& data, const int gameTick);
  msg_vector on_game_init(const jsoncons::json& data, const int gameTick);
  msg_vector on_game_start(const jsoncons::json& data, const int gameTick);
  msg_vector on_turbo(const jsoncons::json& data, const int gameTick);
  msg_vector on_turbo_start(const jsoncons::json& data, const int gameTick);
  msg_vector on_turbo_end(const jsoncons::json& data, const int gameTick);
  msg_vector on_spawn(const jsoncons::json& data, const int gameTick);
  msg_vector on_finish(const jsoncons::json& data, const int gameTick);
  msg_vector on_lap_finished(const jsoncons::json& data, const int gameTick);
  msg_vector on_car_positions(const jsoncons::json& data, const int gameTick);
  msg_vector on_crash(const jsoncons::json& data, const int gameTick);
  msg_vector on_game_end(const jsoncons::json& data, const int gameTick);
  msg_vector on_error(const jsoncons::json& data, const int gameTick);

  bool next_throttle(double* throttle, bool* useTurbo);
  bool next_lane(int carIndex, const jsoncons::json& position_data, std::string* lane);
};

#endif
