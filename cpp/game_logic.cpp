#include "game_logic.h"
#include "protocol.h"

#define CSV_QUOTE "\""
#define CSV_SEP "\",\""

using namespace hwo_protocol;

std::string game_logic::LEFT_TURN_STR = "Left";
std::string game_logic::RIGHT_TURN_STR = "Right";

game_logic::game_logic()
  : action_map
    {
      { "join", &game_logic::on_join },
      { "joinRace", &game_logic::on_join_race },
      { "createRace", &game_logic::on_join_race },
      { "yourCar", &game_logic::on_your_car },
      { "turboAvailable", &game_logic::on_turbo },
      { "turboStart", &game_logic::on_turbo_start },
      { "turboEnd", &game_logic::on_turbo_end },
      { "gameInit", &game_logic::on_game_init },
      { "gameStart", &game_logic::on_game_start },
      { "finish", &game_logic::on_finish },
      { "lapFinished", &game_logic::on_lap_finished },
      { "carPositions", &game_logic::on_car_positions },
      { "crash", &game_logic::on_crash },
      { "gameEnd", &game_logic::on_game_end },
      { "error", &game_logic::on_error }
    },
    m_track_data(nullptr)
{
  
#ifdef CSV_LOGGING
  m_csv_log.open("race_data.csv");
  m_csv_log <<
    CSV_QUOTE <<
    "gameTick" <<
    CSV_SEP <<
    "lastThrottle" <<
    CSV_SEP <<
    "distance" <<
    CSV_SEP <<
    "slipAngle" <<
    CSV_SEP <<
    "currentPieceRadius"
    CSV_SEP <<    
    "currentPieceAngle"
    CSV_QUOTE <<
    std::endl;
#endif
  
}

game_logic::~game_logic()
{

#ifdef CSV_LOGGING
  m_csv_log.close();
#endif
  
  if (m_track_data != nullptr) {
    delete m_track_data;
  }
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  const auto& gameTick = msg.get("gameTick", -1).as<int>();
  
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end()) {
    return (action_it->second)(this, data, gameTick);
  } else {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    return { make_ping() };
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data, const int gameTick)
{
  std::cout << "[MSG] Joined " << data["key"] << " as " << data["name"] << std::endl;

  m_queued_switch_index = -1;
  m_last_throttle = 0.d;
  m_race_started = false;
  
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_join_race(const jsoncons::json& data, const int gameTick)
{
  std::cout << "[MSG] Joined Race " <<
    data["botId"]["key"] <<
    " as " <<
    data["botId"]["name"] <<
    " with car count " <<
    data["carCount"] <<
    std::endl;

  m_queued_switch_index = -1;
  m_last_throttle = 0.d;
  m_race_started = false;
  
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data, const int gameTick)
{
  std::cout << "[MSG] Car: " << data["name"] << std::endl;

  m_car_name = data["name"].as<std::string>();

  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data, const int gameTick)
{
  std::cout << "[MSG] Race started" << std::endl;

  m_race_started = true;
  
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data, const int gameTick)
{
  std::cout << "[MSG] Game init" << std::endl;

  #ifdef DECISION_LOGGING
  std::cout << "  Track: " << jsoncons::pretty_print(data["race"]["track"]) << std::endl;
  #endif
  
  m_track_data = new jsoncons::json(data);
  m_turbo_ready = false;
  m_turbo_on = false;
  m_total_laps = (*m_track_data)["race"]["raceSession"]["laps"].as<int>();
  
  int numLanes = ((*m_track_data)["race"]["track"]["lanes"]).size();
  m_track_sections = new std::vector<track_section>[numLanes];
  
  // Preprocess and decide on optimal lane for each tile

  const jsoncons::json& arrPieces = (*m_track_data)["race"]["track"]["pieces"];
  int numPieces = arrPieces.size();
  m_optimal_lanes = new int[numPieces];
  m_lookahead_angles = new double[numPieces];

  for (int idx = 0; idx < numPieces; ++idx) {

    // Lookahead four pieces

    m_lookahead_angles[idx] = arrPieces[idx].get("angle", 0.0d).as<double>();
    m_lookahead_angles[idx] += arrPieces[(idx + 1) % numPieces].get("angle", 0.0d).as<double>();
    m_lookahead_angles[idx] += arrPieces[(idx + 2) % numPieces].get("angle", 0.0d).as<double>();
    m_lookahead_angles[idx] += arrPieces[(idx + 3) % numPieces].get("angle", 0.0d).as<double>();

    // If the turn was large enough, we want to be on the inside
    
    if (m_lookahead_angles[idx] >= 135.0d) {
      m_optimal_lanes[idx] = numLanes - 1;
    } else if (m_lookahead_angles[idx] <= -135.0d) {
      m_optimal_lanes[idx] = 0;
    } else {

      // -1 indicates we don't care
      
      m_optimal_lanes[idx] = -1;
    }

    #ifdef DECISION_LOGGING
    std::cout << "  Desired lane for " << idx << " is " << m_optimal_lanes[idx] << std::endl;
    #endif
  }

  // Preprocess the track into sections

  int start_piece_idx = 0;
  int end_piece_idx;
  bool classification_done = false;
  do {

    end_piece_idx = start_piece_idx;
      
    // Try to match a straight
    
    if (arrPieces[start_piece_idx].has_member("length")) {

      // Keep going until we hit a non-straight

      double total_length = 0.d;

      do {
        total_length += arrPieces[end_piece_idx]["length"].as<double>();
        end_piece_idx = (end_piece_idx + 1) % arrPieces.size();
      } while (arrPieces[end_piece_idx].has_member("length") && end_piece_idx != 0);

      // Same length for each lane
      for (int laneIdx = 0; laneIdx < numLanes; laneIdx++) {
        m_track_sections[laneIdx].push_back(
          track_section(
            start_piece_idx,
            end_piece_idx,
            laneIdx,
            section_type::STRAIGHT,
            total_length,
            0.d));
      }
    } else {

      // This is a corner of some kind
      // Keep going until a straight or angle sign change

      int direction = (arrPieces[start_piece_idx]["angle"].as<double>() < 0.d) ? -1 : 1;
      double total_angle = 0.d;
      double total_length[numLanes];

      for (int laneIdx = 0; laneIdx < numLanes; laneIdx++) {
        total_length[laneIdx] = 0.d;
      }
      
      do {
        double piece_angle = arrPieces[end_piece_idx]["angle"].as<double>();
        
        if ((direction < 0 && piece_angle < 0.d) ||
            (direction > 0 && piece_angle > 0.d)) {
          total_angle += piece_angle;

          for (int laneIdx = 0; laneIdx < numLanes; laneIdx++) {
            total_length[laneIdx] += game_logic::get_turn_piece_length(
              m_track_data,
              end_piece_idx,
              laneIdx);            
          }

          end_piece_idx = (end_piece_idx + 1) % arrPieces.size();
        } else {
          break;
        }
      } while (arrPieces[end_piece_idx].has_member("angle") && end_piece_idx != 0);

      for (int laneIdx = 0; laneIdx < numLanes; laneIdx++) {

        // TODO: I use angles here, but I realized that doesn't really describe the
        //       tightness of the turn. it should look at radius too!

        // TODO: These constants seem reasonable (or close) for the finland track
        //       so maybe we should work backwards using the radius for those turns
        //       to find some equation that uses the radius as well as angle.
        //       Or just find out the model and solve directly.
        
        double last_turn_angle = 0.d;
        if (m_track_data[laneIdx].size() > 1) {
          last_turn_angle = m_track_sections[laneIdx][m_track_sections[laneIdx].size() - 1].total_angle;
        }
        
        double max_velocity;
        double abs_total_angle = std::abs(total_angle);
        if (abs_total_angle < 45.d) {

          // Shallow turns just floor it man
          
          max_velocity = 10.d;
        } else if (abs_total_angle < 90.d) {

          // Medium turns, a bit higher that normal is cool
          
          max_velocity = 7.5d;
        } else if (abs_total_angle < 135.d) {
          
          // Medium turns, a bit higher that normal is cool

          max_velocity = 6.5d;
          
        } else {

          // Big turn don't fuck it up
          
          max_velocity = 6.d;
          
        }

        // Opposite direction turns we can kick it out a bit more

        if ((last_turn_angle < 0.d && total_angle > 0.d) ||
            (last_turn_angle > 0.d && total_angle < 0.d)) {
          max_velocity *= 1.1d;
        }
                
        m_track_sections[laneIdx].push_back(
          track_section(
            start_piece_idx,
            end_piece_idx,
            laneIdx,
            section_type::TURN,
            total_length[laneIdx],
            total_angle,
            max_velocity));
      }
    }

    start_piece_idx = end_piece_idx;
  } while (start_piece_idx != 0);

  for (int laneIdx = 0; laneIdx < numLanes; laneIdx++) {
    std::cout << "Track analysis lane " << laneIdx << std::endl;
    for (auto it = m_track_sections[laneIdx].begin(); it != m_track_sections[laneIdx].end(); ++it) {      
      std::cout << "  " << (*it) << std::endl;
    }
  }

  m_current_track_section = 0;
    
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_finish(const jsoncons::json& data, const int gameTick)
{
  std::cout << "[MSG] Finish: " << data["name"] << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_lap_finished(const jsoncons::json& data, const int gameTick)
{
  std::cout << "[MSG] Lap finished: " << data["car"]["name"] << " " << data["lapTime"] << std::endl;
  return { make_ping() };
}

void car_position::update_from_json(const jsoncons::json& car_data) {
  angle = car_data["angle"].as<double>();
  piece_index = car_data["piecePosition"]["pieceIndex"].as<int>();
  in_piece_distance = car_data["piecePosition"]["inPieceDistance"].as<double>();
  start_lane_index = car_data["piecePosition"]["lane"]["startLaneIndex"].as<int>();
  end_lane_index = car_data["piecePosition"]["lane"]["endLaneIndex"].as<int>();
  lap = car_data["piecePosition"]["lap"].as<int>();
}

double game_logic::get_turn_piece_length(jsoncons::json* track_data, int piece_index, int lane_index) {
  const jsoncons::json piece = (*track_data)["race"]["track"]["pieces"][piece_index];
  const jsoncons::json lane = (*track_data)["race"]["track"]["lanes"][lane_index];
  
  double radius = piece["radius"].as<double>();
  double pieceAngle = piece["angle"].as<double>();
  double distanceFromCenter = lane["distanceFromCenter"].as<double>();
      
  if (pieceAngle < 0.d) {
    radius += distanceFromCenter;
  } else {
    radius -= distanceFromCenter;
  }

  return std::abs(pieceAngle) * M_PI * radius / 180.d;
}

double car_position::calculate_distance(jsoncons::json* track_data, const car_position& previous_position) {

  // TODO: We could also return rate of change for slip angle as well
  
  const jsoncons::json piece_data = (*track_data)["race"]["track"]["pieces"][piece_index];
  const jsoncons::json previous_piece_data = (*track_data)["race"]["track"]["pieces"][previous_position.piece_index];
  const jsoncons::json lanes = (*track_data)["race"]["track"]["lanes"];
  
  double distance;
  if (piece_index == previous_position.piece_index) {

    // Within the same piece
    
    distance = in_piece_distance - previous_position.in_piece_distance;    
  } else {

    // Assuming that we'll never be skipping an entire piece between updates,
    // figure out the total distance.

    double previous_piece_length;
    if (previous_piece_data.has_member("length")) {

      // Straight piece
      
      previous_piece_length = previous_piece_data["length"].as<double>();
    } else {

      // Angle piece

      previous_piece_length = game_logic::get_turn_piece_length(
        track_data,
        previous_position.piece_index,
        previous_position.end_lane_index);
    }
    
    distance = previous_piece_length -
      previous_position.in_piece_distance +
      in_piece_distance;
    
  }

  return distance;  
}

game_logic::msg_vector game_logic::on_turbo(const jsoncons::json& data, const int gameTick)
{
  std::cout << "[MSG] Turbo in tick " << gameTick << std::endl;  
  m_turbo_ready = true;

  return { make_ping() };
}

game_logic::msg_vector game_logic::on_turbo_start(const jsoncons::json& data, const int gameTick)
{
  std::cout << "[MSG] Turbo start in tick " << gameTick << std::endl;  
  m_turbo_on = true;

  return { make_ping() };
}

game_logic::msg_vector game_logic::on_turbo_end(const jsoncons::json& data, const int gameTick)
{
  std::cout << "[MSG] Turbo end in tick " << gameTick << std::endl;  
  m_turbo_on = false;

  return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data, const int gameTick)
{

  if (! m_race_started) {
    return { make_throttle(1.0d) };
  }

  double new_throttle_value(0.0d);
  bool use_turbo(false);
  std::string new_lane_value;

  // Find our car in the list

  int car_index;
  jsoncons::json car_data;
  for (car_index = 0; car_index < data.size(); ++car_index) {
    if (data[car_index]["id"]["name"].as<std::string>().compare(m_car_name) == 0) {
      car_data = data[car_index];
      break;
    }
  }

  m_last_position = m_current_position;

  m_current_position.update_from_json(car_data);
  double distance = m_current_position.calculate_distance(m_track_data, m_last_position);
  m_current_position.velocity = distance;

  #ifdef CSV_LOGGING

  if (gameTick != -1) {
    const jsoncons::json piece_data = (*m_track_data)["race"]["track"]["pieces"][m_current_position.piece_index];
    m_csv_log <<
      CSV_QUOTE <<
      gameTick <<
      CSV_SEP <<
      m_last_throttle <<
      CSV_SEP <<
      distance <<
      CSV_SEP <<
      m_current_position.angle <<
      CSV_SEP <<
      piece_data.get("radius", 0.d).as<double>() <<
      CSV_SEP <<
      piece_data.get("angle", 0.d).as<double>() <<
      CSV_QUOTE <<
      std::endl;
  }

#endif
  
  // Decide what to do
  
  if (next_lane(car_index, data, &new_lane_value)) {
    #ifdef DECISION_LOGGING
    std::cout << "  Attempting switch " << new_lane_value << " at " << m_queued_switch_index << std::endl;
    #endif
    return { make_switch_lane(new_lane_value) };
  } else if (next_throttle(&new_throttle_value, &use_turbo)) {

    if (use_turbo && m_turbo_ready) {
      return { make_turbo() };
    } else {
      return { make_throttle(new_throttle_value) };
    }
    
  }
    
  //default
  return { make_throttle(0.5d) };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data, const int gameTick)
{

  if (data["name"].as<std::string>().compare(m_car_name) == 0) {    
    std::cout << "[MSG] Crash in tick " << gameTick << " " << data["name"] << " angle " << m_current_position.angle << std::endl;
  } else {
    std::cout << "[MSG] Crash " << data["name"] << std::endl;
  }
  
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& data, const int gameTick)
{
  std::cout << "[MSG] Spawn " << data["name"] << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data, const int gameTick)
{
  std::cout << "[MSG] Race ended " << data["results"] << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data, const int gameTick)
{
  std::cout << "[MSG] Error: " << data.to_string() << std::endl;
  return { make_ping() };
}

bool game_logic::next_throttle(double* throttle, bool* useTurbo)
{
  
  double new_throttle = 1.d;

  track_section current_section = m_track_sections[m_current_position.start_lane_index][m_current_track_section];
  if (current_section.end_index == m_current_position.piece_index) {
    m_current_track_section++;
    m_current_track_section = m_current_track_section % m_track_sections[m_current_position.start_lane_index].size();    
    current_section = m_track_sections[m_current_position.start_lane_index][m_current_track_section];
    std::cout << "  Current section " << m_current_track_section << std::endl;

    if (m_current_position.velocity > current_section.max_entry_speed) {
      std::cout << "WARN entry speed exceeded by " <<
        (m_current_position.velocity - current_section.max_entry_speed) <<
        " for section " << m_current_track_section << std::endl;
    }
    
  }

  track_section next_section = m_track_sections[m_current_position.start_lane_index][
    (m_current_track_section + 1) % m_track_sections[m_current_position.start_lane_index].size()];


  bool new_use_turbo = false;
  int section_pieces = current_section.end_index - current_section.start_index;
  
  if (m_current_position.velocity < next_section.max_entry_speed) {

    // We aren't going fast enough yet
    
    if (current_section.type == section_type::STRAIGHT) {
      new_throttle = 1.0d;

      // Test for turbo, if we have a large enough runway and
      // we are at the start of that runway, turbo time.
      
      // TODO: should check turbo tick length and compute runway
      //       based on that

      double total_length = current_section.total_length;
      if (next_section.type == section_type::STRAIGHT) {
        total_length += next_section.total_length;
      }
      
      if (m_current_position.piece_index == current_section.start_index && total_length > 800.d) {
        new_use_turbo = true;
      }
      
    } else if (current_section.type == section_type::TURN) {

      // This logic is for testing if we are over a third way through the turn,
      // if we are then apply full throttle
      
      if (section_pieces > 0 &&
          m_current_position.piece_index == std::floor(
            ((double)current_section.end_index - (double)section_pieces / 3.d) + 0.5d)) {
        
        new_throttle = 1.0d;
      } else {

        // TODO: This should defer to the PID to figure out the optimal
        //       throttle to maintain ideal turn angle?
        
        new_throttle = 0.75d;
        
      }
    }

    // TODO: Don't just take into account the current angle, but the rate at which it is changing
    
    if (std::abs(m_current_position.angle) > 40.d) {

      // Calm down
      
      new_throttle *= 0.7d;  
    }
  
  } else {

    // TODO: Should be a function of current velocity vs delta not just constant stops

    double velocity_delta = m_current_position.velocity - next_section.max_entry_speed;
    if (velocity_delta > 2.d) {
      new_throttle = 0.0d;
    } else if (velocity_delta > 1.d) {
      new_throttle = 0.1d;
    } else {
      new_throttle = 0.3d;      
    }

  }


  // Check for the final straight on final lap and just floor it

  if ((m_current_position.lap == m_total_laps - 1) && current_section.end_index == 0) {
    new_use_turbo = true;
    new_throttle = 1.0d;
  } else if (m_turbo_on) {
    
    // TODO: Use model to actually figure this out
    // It is too powerful! This is tuned for the finland track, it should
    // actually take into acount the run length and find an optimal throttle
    // to deal with the engine multipler, probably decreasing over time instead of this.
  
    new_throttle *= 0.8d;
    
  }

  if (m_last_position.piece_index != m_current_position.piece_index) {
    printf("  Tile %2i: Throttle: %2.2f Car angle: % 5.1f\n",
           m_current_position.piece_index, new_throttle, m_current_position.angle);
    if (new_use_turbo) {
      printf("  TURBO\n");
    }
  }

  (*throttle) = new_throttle;
  (*useTurbo) = new_use_turbo;
  m_last_throttle = new_throttle;
  return true;
}

bool game_logic::next_lane(int carIndex, const jsoncons::json& position_data, std::string* lane)
{
  
  // Find the next corner, and see if we need to switch to be on the inside

  int currentPieceIndex = position_data[carIndex]["piecePosition"]["pieceIndex"].as<int>();
  int currentLaneIndex = position_data[carIndex]["piecePosition"]["lane"]["startLaneIndex"].as<int>();
  int desiredLaneIndex = currentLaneIndex;
  
  // If we just got to the queued switch, reset the flag
  // to look for the next one
  
  if (currentPieceIndex == m_queued_switch_index) {
    m_queued_switch_index = -1;
  }

  const jsoncons::json& arrPieces = (*m_track_data)["race"]["track"]["pieces"];
  int numLanes = ((*m_track_data)["race"]["track"]["lanes"]).size();
  int numPieces = arrPieces.size();

  // Look ahead for a switch piece. If we find one, start running
  // through the list of optimal lanes until we find one

  int nextSwitchIndex = -1;
  int idxPiece = currentPieceIndex + 1;
  do {
    if (nextSwitchIndex == -1) {
      if (arrPieces[idxPiece].get("switch", false).as<bool>()) {
        nextSwitchIndex = idxPiece;

        // Skipping the increment here so that we run the else check
        // on this same index
      
        continue;
        
      }
    } else {
      if (m_optimal_lanes[idxPiece] != -1) {

        // Switch to the optimal lane
        
        desiredLaneIndex = m_optimal_lanes[idxPiece];
        break;
      }
    }

    idxPiece = (idxPiece + 1) % numPieces;
  } while (idxPiece != currentPieceIndex);

  // Make the switch if it is possible, and if we don't already have one queued

  if (desiredLaneIndex != currentLaneIndex && m_queued_switch_index == -1) {
    if (desiredLaneIndex > currentLaneIndex) {    
      lane->assign(RIGHT_TURN_STR);
      m_queued_switch_index = nextSwitchIndex;
      return true;
    } else if (desiredLaneIndex < currentLaneIndex) {
      lane->assign(LEFT_TURN_STR);
      m_queued_switch_index = nextSwitchIndex;
      return true;
    }
  } else {
    return false;
  }
  
}
